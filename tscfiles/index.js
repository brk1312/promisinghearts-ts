"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const path = require("path");
const app = express();
const port = 3000; // default port to listen
app.set("views", path.join(__dirname.substr(0, __dirname.lastIndexOf("/")) + "src/views"));
app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname.substr(0, __dirname.lastIndexOf("/")), "bower_components")));
app.use(express.static(path.join(__dirname.substr(0, __dirname.lastIndexOf("/")), "plugins")));
app.use(express.static(path.join(__dirname.substr(0, __dirname.lastIndexOf("/")), "dist")));
// define a route handler for the default home page
app.get("/", (req, res) => {
    res.render("index");
});
// start the Express server
app.listen(port, () => {
    // console.log( `server started at http://localhost:${ port }` );
});
//# sourceMappingURL=index.js.map